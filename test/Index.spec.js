import { createLocalVue, shallowMount  } from "@vue/test-utils";
import Index from "@/pages/index"

describe("Index", () => {
  const localVue = createLocalVue()
  let wrapper

  beforeEach(() => {
    wrapper = shallowMount(Index, {
      localVue
    })
  })

  test('Start game and rolling the slots / points smaller than 40', async () => {
    await wrapper.setData({points: 70,rollCounter: 0})
    await wrapper.find('[data-testId="start-game"]').vm.$emit('click')
    expect(wrapper.vm.$data.rollCounter).toBeGreaterThanOrEqual(1)
  })
  test('Start game and rolling the slots / points between 40 - 60', async () => {
    await wrapper.setData({points: 70,rollCounter: 0})
    await wrapper.find('[data-testId="start-game"]').vm.$emit('click')
    expect(wrapper.vm.$data.rollCounter).toBeGreaterThanOrEqual(1)
  })
  test('Start game and rolling the slots / points more then 60', async () => {
    await wrapper.setData({points: 70,rollCounter: 0})
    await wrapper.find('[data-testId="start-game"]').vm.$emit('click')
    expect(wrapper.vm.$data.rollCounter).toBeGreaterThanOrEqual(1)
  })
})
